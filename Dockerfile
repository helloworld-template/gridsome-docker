FROM node:14.17-alpine3.10

WORKDIR /app
# Bundle app source
COPY ./*.js /app/
COPY ./package.json /app/

RUN yarn global add @vue/cli-init
RUN yarn global add @gridsome/cli
RUN yarn install

RUN npm install -g http-server

# start script at docker-compose
